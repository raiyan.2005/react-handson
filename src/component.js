import React from 'react';
import ReactDOM from 'react-dom'
import faker from 'faker'
import CommentDetail from './components/CommentDetail';
import ApprovalCard from './components/ApprovalCard'

const App = () => {
    return (
        <div className="ui container comments">
            <ApprovalCard>
                <CommentDetail author="Ferdous" timeAgo="Today at 6.00 PM" avatar={ faker.image.avatar() } content="Like it!"/>
            </ApprovalCard>
            <ApprovalCard>
                <CommentDetail author="Shifa" timeAgo="Today at 8.00 PM" avatar={ faker.image.avatar() } content="The writing it!"/>
            </ApprovalCard>
            <ApprovalCard>
                <CommentDetail author="Elma" timeAgo="Today at 12.10 PM" avatar={ faker.image.avatar() } content="Like it!"/>
            </ApprovalCard>
            <CommentDetail author="Tahmid" timeAgo="Yesterday at 6.00 PM" avatar={ faker.image.avatar() } content="Nice think it!"/>
        </div>
    );
}

ReactDOM.render(<App/>, document.querySelector('#root'));